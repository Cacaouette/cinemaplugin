package tim.prod.main.charactersystem;

import net.minecraft.server.v1_16_R3.*;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_16_R3.CraftWorld;


public class MachineCharacter extends EntityInsentient implements Character{

    private CharacterInfo info;


    protected MachineCharacter(Location loc, EntityTypes<? extends EntityInsentient> associatedMinecraftType, CharacterInfo info) {
        super(associatedMinecraftType, ((CraftWorld) loc.getWorld()).getHandle());
        this.info = info;
        this.setPosition(loc.getX(), loc.getY(),loc.getZ());
        this.setInvulnerable(false);
    }

}
