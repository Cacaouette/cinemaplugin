package tim.prod.main.charactersystem;

import tim.prod.main.charactersystem.properties.*;

public class CharacterInfo {
    private String name;
    private Race race;
    private State state;
    private Role role;
    private boolean isPlayer;
    private Side side;
    private Origin origin;

    public CharacterInfo(String name, Race race, boolean isPlayer, Origin origin) {
        this.name = name;
        this.race = race;
        this.isPlayer = isPlayer;
        this.origin = origin;
    }

    public boolean isPlayer() {
        return this.isPlayer;
    }

    public Race getRace() {
        return this.race;
    }

    public String getName() {
        return name;
    }

    public Origin getOrigin() {
        return this.origin;
    }
}
