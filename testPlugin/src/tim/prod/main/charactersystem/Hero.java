package tim.prod.main.charactersystem;

import net.minecraft.server.v1_16_R3.EntityInsentient;
import net.minecraft.server.v1_16_R3.EntityTypes;
import org.bukkit.Location;
import org.w3c.dom.Entity;
import tim.prod.main.charactersystem.Character;
import tim.prod.main.charactersystem.MachineCharacter;
import tim.prod.main.charactersystem.CharacterInfo;
import tim.prod.main.charactersystem.powers.Power;

public class Hero implements EntityCharacter {

    private Power powers[];
    private Character character;

    public Hero(Character character, Power... powers) {
        this.character = character;
        this.powers = powers;
    }

    @Override
    public boolean hasPower() {
        return true;
    }

    @Override
    public Character getCharacter() {
        return this.character;
    }
}
