package tim.prod.main.charactersystem.exceptions;

public class RaceDoesNotExistException extends Exception {

    public RaceDoesNotExistException(String message) {
        super(message);
    }
}
