package tim.prod.main.charactersystem.exceptions;

public class OriginDoesNotExistException extends Exception{

    public OriginDoesNotExistException(String message) {
        super(message);
    }
}
