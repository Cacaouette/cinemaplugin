package tim.prod.main.charactersystem.properties;

public enum Role {
    Archer,
    Warrior,
    Peasant
}
