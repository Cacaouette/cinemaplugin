package tim.prod.main.charactersystem.properties;

public enum Side {
    Light,
    Dark,
    Neutral
}
