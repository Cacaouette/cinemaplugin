package tim.prod.main.charactersystem.properties;

public enum Origin {
    Hero,
    Titan,
    Nobody
}
