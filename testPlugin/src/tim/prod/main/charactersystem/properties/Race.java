package tim.prod.main.charactersystem.properties;

public enum Race {
    Elf,
    Orc,
    ForestElf,
    Giant
}

