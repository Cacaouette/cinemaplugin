package tim.prod.main.charactersystem;

import com.mojang.authlib.GameProfile;
import net.minecraft.server.v1_16_R3.EntityPlayer;
import net.minecraft.server.v1_16_R3.MinecraftServer;
import net.minecraft.server.v1_16_R3.PlayerInteractManager;
import net.minecraft.server.v1_16_R3.WorldServer;
import org.bukkit.entity.Player;

import java.lang.reflect.Method;

public class PlayerCharacter implements Character{

    private CharacterInfo info;
    private Player player;

    public PlayerCharacter(Player player, CharacterInfo info) {
        this.player = player;
        this.info = info;
    }

    public Player getPlayer() {
        return this.player;
    }


}
