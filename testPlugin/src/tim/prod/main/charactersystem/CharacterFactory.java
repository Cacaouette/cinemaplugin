package tim.prod.main.charactersystem;

import com.sun.istack.internal.NotNull;
import net.minecraft.server.v1_16_R3.*;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_16_R3.CraftWorld;
import org.bukkit.entity.Player;
import tim.prod.main.charactersystem.exceptions.OriginDoesNotExistException;
import tim.prod.main.charactersystem.powers.Power;
import tim.prod.main.charactersystem.powers.PowerControl;
import tim.prod.main.charactersystem.properties.Race;
import tim.prod.main.charactersystem.exceptions.RaceDoesNotExistException;

import java.util.HashMap;

public class CharacterFactory {

    public CharacterFactory() {}

    private static HashMap<Player,EntityCharacter> as1;
    private static HashMap<EntityLiving,EntityCharacter> as2;

    public static void initCharacterFactoryAssociations() {
        as1 = new HashMap<Player,EntityCharacter>();
        as2 = new HashMap<EntityLiving,EntityCharacter>();
    }

    public static EntityCharacter getEntityCharacter(Player player) {
        return as1.get(player);
    }

    public static EntityCharacter getEntityCharacter(EntityLiving entityLiving) {
        return as2.get(entityLiving);
    }

    public static Race stringToRace(String stringRace) throws RaceDoesNotExistException{
        switch(stringRace.toUpperCase()) {
            case "ELF": return Race.Elf;
            case "FORESTELF": return Race.ForestElf;
            case "GIANT": return Race.Giant;
            case "ORC": return Race.Orc;
            default: throw new RaceDoesNotExistException("Race does not exist");
        }
    }

    private static Character createCharacter(@NotNull CharacterInfo info, Player player) {
        if(info.isPlayer()) return new PlayerCharacter(player,info);
        MachineCharacter machineCharacter = new MachineCharacter(player.getLocation(), initEntity(info),info);
        customizeRace(machineCharacter,info);
        WorldServer world = ((CraftWorld) player.getWorld()).getHandle();
        world.addEntity(machineCharacter);
        return machineCharacter;
    }

    public static EntityCharacter createEntityCharacter(CharacterInfo info, Player player, Power... powers) throws OriginDoesNotExistException {
        switch(info.getOrigin()) {
            case Hero:  {
                Character character = createCharacter(info,player);
                Hero hero = new Hero(character,powers);
                if(info.isPlayer()) {
                    PlayerCharacter p = (PlayerCharacter) character;
                    for(Power power : powers) {
                        p.getPlayer().getInventory().setItem(5, PowerControl.getArtifact(power).getMinecraftArtifact());
                    }
                    as1.put(player,hero);
                }
                return hero;
            }
            case Nobody: return new Nobody(createCharacter(info,player));
            default: throw new OriginDoesNotExistException("Origin does not exist");
        }
    }

    private static EntityTypes<? extends EntityInsentient> initEntity(@NotNull CharacterInfo info) {
        switch(info.getRace()) {
            case Elf: return EntityTypes.PIGLIN;
            case Orc: return EntityTypes.ZOMBIE;
            case Giant: return EntityTypes.GIANT;
            case ForestElf: return EntityTypes.SKELETON;
            default: return EntityTypes.SHEEP;
        }
    }

    private static void customizeRace(MachineCharacter machineCharacter, CharacterInfo info) {
        switch (info.getRace()) {
            case Elf: {
                machineCharacter.setCustomName(new ChatComponentText("Elf"));
                break;
            }
            case Orc: {
                machineCharacter.setCustomName(new ChatComponentText(ChatColor.RED + "Orc"));
                break;
            }
            case ForestElf: {
                machineCharacter.setCustomName(new ChatComponentText(ChatColor.BLUE + "Forest Elf"));
                break;
            }
            case Giant: {
                machineCharacter.setCustomName(new ChatComponentText(ChatColor.GREEN + "Giant"));
                break;
            }
            default: {
                machineCharacter.setCustomName(new ChatComponentText("WTF????"));
            }
        }
    }
}
