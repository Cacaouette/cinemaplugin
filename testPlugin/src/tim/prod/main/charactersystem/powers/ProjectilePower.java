package tim.prod.main.charactersystem.powers;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import tim.prod.main.charactersystem.Character;
import tim.prod.main.charactersystem.PlayerCharacter;

import java.util.Vector;

public abstract class ProjectilePower extends Power{

    private double range;


    public ProjectilePower(String name) {
        super(name);
    }

    public void setRange(double range) {
        this.range = range;
    }

    public Location applyLocation(Location location, Character character) {
        double x = location.getX();
        double y = location.getY();
        double z = location.getZ();
        double d = Math.sqrt(Math.pow(x,2) + Math.pow(y,2) + Math.pow(z,2));
        if(d > range) {

            if(character instanceof PlayerCharacter) {
                Player player = ((PlayerCharacter) character).getPlayer();
                double signX = player.getLocation().getX() < x ? 1 : (-1);
                double signY = player.getLocation().getY() < y ? 1 : (-1);
                double signZ = player.getLocation().getZ() < z ? 1 : (-1);
                location.setX(player.getLocation().getX() +  signX * range / Math.sqrt(3));
                location.setY(player.getLocation().getY() + signY * range / Math.sqrt(3));
                location.setZ(player.getLocation().getZ() + signZ * range / Math.sqrt(3));
            }
        }
        return location;
    }
}
