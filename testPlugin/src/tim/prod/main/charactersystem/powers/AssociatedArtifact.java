package tim.prod.main.charactersystem.powers;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Objects;

public class AssociatedArtifact {

    private String name;
    private ItemStack minecraftArtifact;

    protected AssociatedArtifact(String name, ItemStack item) {
        this.name = name;
        this.minecraftArtifact = item;
    }

    public ItemStack getMinecraftArtifact() {
        return  this.minecraftArtifact;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AssociatedArtifact that = (AssociatedArtifact) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(minecraftArtifact, that.minecraftArtifact);
    }

    @Override
    public int hashCode() {
        return this.name.hashCode();
    }

    public ItemMeta getArtifactMeta() {
        return this.minecraftArtifact.getItemMeta();
    }

    public void setArtifactMeta(ItemMeta meta) {
        this.minecraftArtifact.setItemMeta(meta);
    }
}
