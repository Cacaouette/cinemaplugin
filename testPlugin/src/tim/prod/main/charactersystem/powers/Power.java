package tim.prod.main.charactersystem.powers;


import org.bukkit.entity.Entity;
import tim.prod.main.charactersystem.EntityCharacter;

import java.util.Objects;

public abstract class Power {
    private String name;

    public String getName() {
        return this.name;
    }

    public abstract void execute(EntityCharacter executor, Entity target);

    public Power(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !(o instanceof Power)) return false;
        Power power = (Power) o;
        return this.name.equals(power.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
