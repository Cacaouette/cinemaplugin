package tim.prod.main.charactersystem.powers;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import tim.prod.main.charactersystem.Character;
import tim.prod.main.charactersystem.EntityCharacter;
import tim.prod.main.charactersystem.PlayerCharacter;

public class Teleportation extends ProjectilePower {
    public Teleportation(String name) {
        super(name);
        setRange(20.0);
    }

    @Override
    public void execute(EntityCharacter executor, Entity target) {
        if(executor.hasPower()) {
            Character character = executor.getCharacter();
            if(character instanceof PlayerCharacter) {
                PlayerCharacter playerCharacter = (PlayerCharacter) character;
                Player player = playerCharacter.getPlayer();
                Location loc  = player.getEyeLocation();
                player.teleport(applyLocation(loc,character));
            }
        }
    }
}
