package tim.prod.main.charactersystem.powers;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class PowerControl {

    private static HashMap<Power,AssociatedArtifact> powerRealisationAssociation;
    private static HashMap<String, Power> ip;

    public static void display(Player player) {
        player.sendMessage("Hi");
        Set<String> stack = ip.keySet();
        Iterator<String> it = stack.iterator();
        while(it.hasNext()) {
            String item = it.next();
            player.sendMessage(ip.get(item).getName());
        }
    }

    public static void initPowerAndArtifactsAssociations() {
        powerRealisationAssociation = new HashMap<Power,AssociatedArtifact>();
        ip = new HashMap<String,Power>();
        Power power = new Teleportation("teleportation");
        ItemStack item = new ItemStack (Material.DIAMOND,1);
        ItemMeta meta = item.getItemMeta();
        meta.addEnchant(Enchantment.FIRE_ASPECT, 1, false);
        meta.setLocalizedName("teleporter");
        item.setItemMeta(meta);
        AssociatedArtifact artifact = new AssociatedArtifact(meta.getLocalizedName(), item);
        powerRealisationAssociation.put(power,artifact);
        ip.put(item.getItemMeta().getLocalizedName(),power);
    }

    public static AssociatedArtifact getArtifact(Power power) {
        return powerRealisationAssociation.get(power);
    }

    public static Power getPower(String itemName) {
        return ip.get(itemName);
    }
}
