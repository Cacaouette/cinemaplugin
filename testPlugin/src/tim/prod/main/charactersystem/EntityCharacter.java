package tim.prod.main.charactersystem;

public interface EntityCharacter {

    public boolean hasPower();

    public Character getCharacter();
}
