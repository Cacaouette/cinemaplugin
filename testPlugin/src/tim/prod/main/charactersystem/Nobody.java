package tim.prod.main.charactersystem;

public class Nobody implements EntityCharacter{
    private Character character;

    public Nobody(Character character) {
        this.character = character;
    }

    @Override
    public boolean hasPower() {
        return false;
    }

    @Override
    public Character getCharacter() {
        return this.character;
    }
}
