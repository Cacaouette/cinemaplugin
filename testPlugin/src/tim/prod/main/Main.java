package tim.prod.main;

import net.minecraft.server.v1_16_R3.Entity;
import net.minecraft.server.v1_16_R3.WorldServer;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_16_R3.CraftWorld;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import tim.prod.main.charactersystem.*;
import tim.prod.main.charactersystem.Character;
import tim.prod.main.charactersystem.powers.AssociatedArtifact;
import tim.prod.main.charactersystem.powers.Power;
import tim.prod.main.charactersystem.powers.PowerControl;
import tim.prod.main.charactersystem.powers.Teleportation;
import tim.prod.main.charactersystem.properties.Origin;

import javax.swing.*;

public class Main extends JavaPlugin implements Listener {


    @Override
    public void onEnable() {
        PowerControl.initPowerAndArtifactsAssociations();
        CharacterFactory.initCharacterFactoryAssociations();
        this.getServer().getPluginManager().registerEvents(this,this);
    }

    @Override
    public void onDisable() {
    }


    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(label.equalsIgnoreCase("sp")) {
            if(!(sender instanceof Player)) {
                return true;
            }
            Player player = (Player) sender;

            player.getInventory().setItem(1,new ItemStack(Material.DIAMOND,1));

            if(args[0].equalsIgnoreCase("ip")) {
                PowerControl.display(player);
                return false;
            }

            String type = args.length >=1 ? args[0] : "Elf";
            try {
                CharacterInfo info = new CharacterInfo("My Bitch", CharacterFactory.stringToRace(type),true, Origin.Hero);
                CharacterFactory.createEntityCharacter(info, player,new Teleportation("teleportation"));
            } catch(Exception e) {
                player.sendMessage(ChatColor.RED + e.getMessage());
            }
            player.sendMessage(ChatColor.GREEN + "It works");
        }

        return false;
    }

    @EventHandler
    public void usePower(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        Action action = event.getAction();
        if(action.equals(Action.RIGHT_CLICK_AIR) || action.equals(Action.RIGHT_CLICK_BLOCK)) {
            player.sendMessage("Clicked");
            EntityCharacter entityCharacter = CharacterFactory.getEntityCharacter(player);
            player.sendMessage(entityCharacter.hasPower() + " ");
            ItemStack item = player.getInventory().getItemInMainHand();
            player.sendMessage(item.getItemMeta().getLocalizedName());
            if(PowerControl.getPower(item.getItemMeta().getLocalizedName()) != null) {
                player.sendMessage("Works");
                PowerControl.getPower(item.getItemMeta().getLocalizedName()).execute(entityCharacter,player.getSpectatorTarget());
            }
        }
    }
}
